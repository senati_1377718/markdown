# EJERCIOS DE CLASE
#### PRIVIELEGIOS
##### **1**) Crear usuario
```sql
use empresadb;
```
```sql
CREATE USER 'gonzalo'@'localhost' IDENTIFIED BY 'admon123';
```
##### **3**) Conceder privilegios
```sql
GRANT SELECT ON EmpresaDB.Departamento TO 'usuarioEjemplo'@'localhost';
GRANT INSERT ON EmpresaDB.Empleado TO 'usuarioEjemplo'@'localhost';
GRANT UPDATE ON EmpresaDB.Proyecto TO 'usuarioEjemplo'@'localhost';
```
##### **4**) Revocar privulegios
```sql
REVOKE SELECT ON EmpresaDB.Departamento FROM 'usuarioEjemplo'@'localhost';
REVOKE INSERT ON EmpresaDB.Empleado FROM 'usuarioEjemplo'@'localhost';
REVOKE UPDATE ON EmpresaDB.Proyecto FROM 'usuarioEjemplo'@'localhost';
```
##### **5**) COMMIT y ROLLBACK:
```sql
START TRANSACTION;
INSERT INTO Departamento (nombredepartamento, ubicacion) VALUES ('IT','SEATTLE');
COMMIT ;
ROLLBACK ;
```
##### **6**) SAVEPOINT:
```sql
START TRANSACTION;
INSERT INTO departamento (NombreDepartamento, Ubicacion) VALUES ('HR','NEW YORK');
SAVEPOINT Savepoint1;
```
```sql
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento)
VALUES ('JOHN DOE', 'MANAGER', 3, '2023-01-01', 3500, NULL, 10);
ROLLBACK TO Savepoint1;
COMMIT;
```
##### **8**) SET TRANSACTION:
```sql
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;
START TRANSACTION;
INSERT INTO proyecto(nombreproyecto, ubicacion, iddepartamento) VALUES ('NEW PROJECT','CHICAGO',10);
COMMIT;
```
##### **9** Creación de la tabla con tipos de datos optimizados y restricciones
```sql
CREATE DATABASE EmpleadoDB;
USE EmpleadoDB;

CREATE TABLE Departamento (
    IDDepartamento INT AUTO_INCREMENT PRIMARY KEY,
    NombreDepartamento VARCHAR(50) NOT NULL UNIQUE,
    Ubicacion VARCHAR(50) DEFAULT 'UNKNOWN'
);
CREATE TABLE Empleado (
    IDEmpleado INT AUTO_INCREMENT PRIMARY KEY,
    NombreEmpleado VARCHAR(50) NOT NULL,
    Puesto VARCHAR(50),
    IDJefe INT,
    FechaContratacion DATE DEFAULT CURRENT_DATE,
    Salario DECIMAL(10,2) CHECK (Salario >= 0),
    Comision DECIMAL(10,2) CHECK (Comision >= 0),
    IDDepartamento INT,
    FOREIGN KEY (IDDepartamento) REFERENCES Departamento(IDDepartamento),
    FOREIGN KEY (IDJefe) REFERENCES Empleado(IDEmpleado)
);
```
##### **10**)
```sql
DELIMITER //
CREATE PROCEDURE ListarEmpleados()
BEGIN
    DECLARE done INT DEFAULT 0;
    DECLARE ID INT;
    DECLARE Nombre VARCHAR(50);
    DECLARE EmpleadoCursor CURSOR FOR SELECT IDEmpleado, NombreEmpleado FROM Empleado;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;
    OPEN EmpleadoCursor;
    REPEAT
        FETCH EmpleadoCursor INTO ID, Nombre;
        IF NOT done THEN
            SELECT ID, Nombre;
        END IF;
    UNTIL done END REPEAT;
    CLOSE EmpleadoCursor;
END//
DELIMITER ;
CALL ListarEmpleados();
```
##### **11**) Alias
```sql
SELECT E.NombreEmpleado AS Nombre, D.NombreDepartamento AS Departamento
FROM Empleado E
JOIN Departamento D ON E.IDDepartamento = D.IDDepartamento;
```
##### **12**) Parametrización
```sql
PREPARE stmt FROM 'SELECT * FROM Empleado WHERE Salario > ?';
SET @SalarioMin = 2500.00;
EXECUTE stmt USING @SalarioMin;
DEALLOCATE PREPARE stmt;
```
##### **13**) EXISTS
```sql
SELECT NombreEmpleado
FROM empleado E
WHERE EXISTS(
    SELECT 1
    FROM empleadoproyecto EP
    WHERE EP.IDEmpleado = E.IDEmpleado
);
```
##### **14**) DISTINCT
```sql
SELECT DISTINCT NombreDepartamento
FROM departamento;
```
##### **15** TOP / LIMIT
```sql
SELECT * FROM Empleado ORDER BY Salario DESC LIMIT 5;
```
##### **16**)  *
```sql
SELECT * FROM empleado;
```
##### **17**) Verificar si existe un registro
```sql
IF ( SELECT EXISTS(SELECT 1 FROM empleado WHERE NombreEmpleado = 'SMITH')) THEN
    SELECT 'Empleado existente';
ELSE
    SELECT 'Empleado no existente';
end if;
```
##### **18**) ORDER BY
```sql
SELECT * FROM empleado ORDER BY  NombreEmpleado ASC;
```